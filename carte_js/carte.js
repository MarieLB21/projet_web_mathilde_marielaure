var map = L.map('mapid').setView([46.52863469527167,2.43896484375], 6);

var test2 = L.tileLayer('http://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}',{
maxZoom: 20,
subdomains:['mt0','mt1','mt2','mt3']
}).addTo(map);


var ObjetFixe = L.Icon.extend({
  options: {
  iconSize: [40, 40],
  iconAnchor: null,
  popupAnchor: [4, 4],
  maxZoom : 15,
  }
});

// Création des icônes des lieux
olivier = new ObjetFixe({iconUrl: 'lieux/olivier.png'});
mont_blanc = new ObjetFixe({iconUrl: 'objets/sommet_mt_blanc.png'});
mont_lure = new ObjetFixe({iconUrl: 'objets/sommet_mt_lure.png'});
digoin = new ObjetFixe({iconUrl: 'objets/chapiteau.png'});
strasbourg = new ObjetFixe({iconUrl: 'objets/eglise.png'});
nord = new ObjetFixe({iconUrl: 'objets/mine_charbon.png'});
bretagne = new ObjetFixe({iconUrl: 'objets/carriere.png'});
paris = new ObjetFixe({iconUrl: 'objets/tour_eiffel.png'});
bordeaux = new ObjetFixe({iconUrl: 'objets/pat.png'});
sancy = new ObjetFixe({iconUrl: 'objets/volcan.png'});

var icon_mont_lure_donnees = {nom:'', cheminacces:'', lat:'', longi:'', zoom:'', bloquepar:'', debloque:''};
var icon_mont_blanc_donnees = {nom:'', cheminacces:'', lat:'', longi:'', zoom:'', bloquepar:'', debloque:''};
var icon_strasbourg_donnees = {nom:'', cheminacces:'', lat:'', longi:'', zoom:'', bloquepar:'', debloque:''};
var icon_nord_donnees = {nom:'', cheminacces:'', lat:'', longi:'', zoom:'', bloquepar:'', debloque:''};
var icon_bretagne_donnees = {nom:'', cheminacces:'', lat:'', longi:'', zoom:'', bloquepar:'', debloque:''};
var icon_paris_donnees = {nom:'', cheminacces:'', lat:'', longi:'', zoom:'', bloquepar:'', debloque:''};
var icon_bordeaux_donnees = {nom:'', cheminacces:'', lat:'', longi:'', zoom:'', bloquepar:'', debloque:''};
var icon_bourgogne_donnees = {nom:'', cheminacces:'', lat:'', longi:'', zoom:'', bloquepar:'', debloque:''};
var icon_sancy_donnees = {nom:'', cheminacces:'', lat:'', longi:'', zoom:'', bloquepar:'', debloque:''};

var icon_gentiane_donnees = {nom:'', cheminacces:'', lat:'', longi:'', zoom:'', bloquepar:'', debloque:'', texte:''};

// Création des marqueurs des lieux
olivier = L.marker([43.959933,5.780712], {icon: olivier}).addTo(map);
// mont_blanc = L.marker([45.832622,6.865175], {icon: mont_blanc});//.addTo(map);
// mont_lure = L.marker([44.1231,5.8026], {icon: mont_lure});//.addTo(map);
// bourgogne = L.marker([46.481976,3.984840], {icon: digoin});//.addTo(map);
// strb = L.marker([48.581880,7.751035], {icon: strasbourg});//.addTo(map);
// mine = L.marker([50.460844, 2.986978], {icon: nord});//.addTo(map);
// bret = L.marker([48.609539,-2.494735], {icon: bretagne});//.addTo(map);
// tour_eiffel = L.marker([48.858370,2.294481], {icon: paris});//.addTo(map);
// canele = L.marker([44.840118,-0.598408], {icon: bordeaux});//.addTo(map);
// sanc = L.marker([45.528387,2.813930], {icon: sancy});//.addTo(map);

// olivier.addEventListener("mousemove", function(){
//  var popupContent = '<p>Bonjour Hatlas, afin de trouver le prochain lieu, voici une petite énigme : Prends de la hauteur en te rendant sur le point culminant proche de Forcalquier</p>';
//  olivier.bindPopup(popupContent).openPopup();
// })

// Premier test clique sur l'olivier
// à savoir que le premier lieu où chercher l'énigme
// apparaitra si l'on clique sur l'olivier
var layerLieux = L.layerGroup();
var layerobjets = L.layerGroup();
olivier.addEventListener("click", function(){
    lieuRecupLieu("forcalquier", 'lieuPrLieu', icon_mont_lure_donnees, layerLieux);
    console.log(icon_mont_lure_donnees);
    console.log(layerLieux);
  //map.on('zoomend',zoom(mont_lure, 10));
  // map.addLayer(mont_lure);
   //mont_lure.addTo(map);
});

// CRéation d'un groupe de couches
var lieux = L.layerGroup([mont_lure, mont_blanc, bourgogne, strb,mine, bret, tour_eiffel, canele, sanc]);


//map.addLayer(cities);
// Fonction zoom pour faire apparaitre les marqueurs
// qu'à partir d'un certain zoom
function zoom(el,zoom){
var zoomlevel = map.getZoom();
  if (zoomlevel <= zoom){
      if (map.hasLayer(el)) {
          map.removeLayer(el);
      }
    else {
          console.log("no point layer active");
      }
  }
  if (zoomlevel > zoom){
      if (map.hasLayer(el)){
          console.log("layer already added");
      } else {
          map.addLayer(el);
      }
  }

console.log("Current Zoom Level =" + zoomlevel)
};

// map.addListener('zoom_changed', function(){
//   var zoomlevel = map.getZoom();
//   if (zoomlevel < 10){
//     if (map.hasLayer(layerLieux)) {
//       map.removeLayer(layerLieux)
//     }else {
//       console.log("no");
//       console.log(zoomlevel);
//     }
//   }
//   if (zoomlevel>=10) {
//     // map.addLayer(layerLieux);
//     console.log(zoomlevel);
//     if (carte.hasLayer(layerLieux)) {
//       console.log("already add");
//     }else {
//       layerLieux.addTo(map);
//     }
//   }
// })


//   map.on('zoomend', zoom);
//   function zoom() {
//   var zoomlevel = map.getZoom();
//   console.log(zoomlevel)
//   if (zoomlevel   >= 12){
//           map.addLayer(mont_lure);
//         }
//   else {
//             console.log("no point layer active");
//         }
//
// };

/* CREATION DE COUCHES
L.layerGroup([marker1, marker2])
.addLayer(polyline)
.addTo(map);
*/



/*
var popup = L.popup()
.setLatLng([43.959933,5.780712])
.setContent('<p>Bonjour Hatlas, afin de trouver le prochain lieu, voici une petite énigme : Prends de la hauteur en te rendant sur le point culminant proche de Forcalquier</p>')
.openOn(map);
*/

mont_lure.addEventListener("click", function(){
  firstAjax();
});


// fonction pour récupérer l'objet bloqué par le lieu pris en entrée
function lieuRecupObjet(val, lieuPrObjet, icon_objet_donnees, layerobjets){
  var data = "lieuPrObjet=" + val;

  fetch('carte.php',{
    method: 'post',
    body: data,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
  .then(r => r.json()
)
  .then(r => {
//traitement à modifier:
    // Création des icônes des lieux
    charbon = new ObjetFixe({iconUrl: r[1]});
    console.log(r[1]);
    // Création des marqueurs objets
    charbon_m = L.marker([r[3],r[4]], {icon: charbon});//.addTo(map);

    charbon.addTo(map);
    //map.on('zoomend',zoom(charbon_m, r2[5]));

  })
};

// fonction pour récupérer le lieu bloqué par le lieu pris en entrée
function lieuRecupLieu(val, lieuPrLieu, icon_lieu_donnees, layerLieux){
  var data = {type: lieuPrLieu, demandeur: val};

  fetch('carte.php',{
    method: 'post',
    body: JSON.stringify(data)
    // headers: {
    //   'Content-Type': 'application/x-www-form-urlencoded'
    // }
  })
  .then(r => r.json()
  )
  .then(r => {
    icon_lieu_donnees.nom = r["nom"];
    icon_lieu_donnees.cheminacces = r["cheminacces"];
    icon_lieu_donnees.lat = r["lat"];
    icon_lieu_donnees.longi = r["longi"];
    icon_lieu_donnees.zoom = r["zoom"];
    icon_lieu_donnees.bloquepar = r["bloquepar"];
    icon_lieu_donnees.debloque = r["debloque"];

    var icone =new ObjetFixe({iconUrl: r["cheminacces"]});
    icone = L.marker([r["lat"],r["longi"]], {icon: icone}).addTo(map);
    layerLieux.addLayer(icone);
    // console.log(icon_lieu_donnees);
    // console.log(r);
//traitement à modifier:
    // var nom = r["nom"];
    // console.log(r);
    // // Création des icônes des lieux
    // icone = new ObjetFixe({iconUrl: r["cheminacces"]});
    // // console.log(r[1]);
    // // Création des marqueurs objets
    // icone_marquer = L.marker([r["lat"],r["longi"]], {icon: r["nom"]});//.addTo(map);
    // //
    // icone.addTo(map);
    // map.on('zoomend',zoom(icone_marquer, r["zoom"]));
//
  })
};

// fonction pour récupérer l'objet par l'objet pris en entrée
function ObjetRecupObjet(val){
  var data = "objetPrObjet=" + val;

  fetch('carte.php',{
    method: 'post',
    body: data,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
  .then(r => r.json()
)
  .then(r => {
//traitement à modifier:
    // Création des icônes des lieux
    charbon = new ObjetFixe({iconUrl: r[1]});
    console.log(r[1]);
    // Création des marqueurs objets
    charbon_m = L.marker([r[3],r[4]], {icon: charbon});//.addTo(map);

    charbon.addTo(map);
    //map.on('zoomend',zoom(charbon_m, r2[5]));

  })
};

// fonction pour récupérer le lieu par l'objet pris en entrée
function ObjetRecupLieu(val){
  var data = "objetPrLieu=" + val;

  fetch('carte.php',{
    method: 'post',
    body: data,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
  .then(r => r.json()
)
  .then(r => {
//traitement à modifier:
    // Création des icônes des lieux
    charbon = new ObjetFixe({iconUrl: r[1]});
    console.log(r[1]);
    // Création des marqueurs objets
    charbon_m = L.marker([r[3],r[4]], {icon: charbon});//.addTo(map);

    charbon.addTo(map);
    //map.on('zoomend',zoom(charbon_m, r2[5]));

  })
};

// fonction pour rentrer les réponses pour les codes des coffres par exemple
function repondre(valeur){
  var suggestion = prompt('Veuillez rentrer votre réponse :');
  while (true){
    if (suggestion == valeur) {
      alert('bonne réponse');
      break;
    }else {
      var suggestion = prompt('Raté, veuillez réessayer !')
    }
  }
}

// Premier test clique sur l'olivier
// à savoir que le premier lieu où chercher l'énigme
// apparaitra si l'on clique sur l'olivier
// var icon_lieu_donnees = {nom:'', cheminacces:'', lat:'', longi:'', zoom:'', bloquepar:'', debloque:''};
// olivier.addEventListener("click", function(){
//     lieuRecupLieu("forcalquier", 'lieuPrLieu', icon_donnees);
//     console.log(icon_donnees);
  //map.on('zoomend',zoom(mont_lure, 10));
  // map.addLayer(mont_lure);
   //mont_lure.addTo(map);
// })
