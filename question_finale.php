<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/style_quest_fin.css">
    <link href="https://fonts.googleapis.com/css?family=Courgette|Satisfy&display=swap" rel="stylesheet">
    <title>Escape Game</title>
  </head>
  <body>
    <!-- <form id="quest" action="fin.php" method="post"> -->
    <form id="quest" action="#" method="post">
      <div id="question">
        <legend id="leg">Une dernière pour la fin !</legend>
        <p id="paragraphe">Pour que Julie puisse recevoir ses cadeaux rapportés par Hatlas, il faut que vous répondiez correctement à cette question :</p>
        <p id="thequestion">Durant quel mois de l’année sont majoritairement récoltées les olives noires?</p>
        <p></p>
        <p class="reps"><label>Février<input id="fevrier" type="checkbox" name="rep"></label></p>
        <p class="reps"><label>Juin<input id="juin" type="checkbox" name="rep"></label></p>
        <p class="reps"><label>Août<input id="aout" type="checkbox" name="rep"></label></p>
        <p class="reps"><label>Novembre<input id="novembre" type="checkbox" name="rep"></label></p>
        <p id="submit"><input id="valider" class="submit" type="submit" name="valider" value="Valider"></p>
      </div>
    </form>
    <script type="text/javascript">
      var boxNovembre = document.getElementById('novembre');
      var boxFevrier = document.getElementById('fevrier');
      var boxJuin = document.getElementById('juin');
      var boxAout = document.getElementById('aout');
      var paragraphe = document.getElementById('paragraphe');
      var form = document.getElementById('quest');
      var submit = document.getElementById('valider');

      form.addEventListener("submit", function(){
        console.log("rentre dans addevent");
        if (boxNovembre.checked == true && boxFevrier.checked == false && boxJuin.checked == false && boxAout.checked == false) {
          form.action = "fin.php";
          form.submit();
          console.log("box checked");
        }else {
          alert("Ce n'est pas la bonne réponse, veuillez réessayer.");
          console.log("box unchecked");
        }
      });
    </script>
  </body>
</html>
