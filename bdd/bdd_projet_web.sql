-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 08, 2019 at 04:55 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `classement`
--

CREATE TABLE `classement` (
  `id` int(10) NOT NULL,
  `pseudo` text,
  `mail` text,
  `temps_debut` time DEFAULT NULL,
  `temps_fin` time DEFAULT NULL,
  `score` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `classement`
--

INSERT INTO `classement` (`id`, `pseudo`, `mail`, `temps_debut`, `temps_fin`, `score`) VALUES
(37, 'Nicolas', 'nicolas.nini@gmail.com', NULL, NULL, '00:05:12'),
(38, 'Lola', 'Aleaulola@hotmail.com', NULL, NULL, '00:07:03'),
(39, 'Macron', 'Macron@elysee.fr', NULL, NULL, '00:15:00');

-- --------------------------------------------------------

--
-- Table structure for table `lieux`
--

CREATE TABLE `lieux` (
  `id` int(3) NOT NULL,
  `nom` text NOT NULL,
  `cheminacces` text NOT NULL,
  `lat` float DEFAULT NULL,
  `longi` float DEFAULT NULL,
  `zoom` float NOT NULL,
  `bloquepar` int(3) NOT NULL,
  `debloque` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lieux`
--

INSERT INTO `lieux` (`id`, `nom`, `cheminacces`, `lat`, `longi`, `zoom`, `bloquepar`, `debloque`) VALUES
(1, 'forcalquier', 'lieux/olivier.png', 43.9599, 5.78071, 10, 0, 2),
(2, 'mont-lure', 'lieux/montagne_lure.png', 44.1231, 5.8026, 10, 1, 1),
(3, 'mont-blanc', 'lieux/sommet_mt_blanc.png', 45.8326, 6.86517, 10, 2, 0),
(4, 'strasbourg', 'lieux/eglise.png', 48.5819, 7.75104, 10, 3, 5),
(5, 'nord', 'lieux/mine_charbon.png', 50.4608, 2.98698, 10, 14, 0),
(6, 'bretagne', 'lieux/carriere.png', 48.6095, -2.49474, 10, 17, 7),
(7, 'paris', 'lieux/tour_eiffel.png', 48.8584, 2.29448, 10, 7, 0),
(8, 'bordeaux', 'lieux/pat.png', 44.8401, -0.598408, 10, 9, 0),
(9, 'bourgogne', 'lieux/chapiteau.png', 46.482, 3.98484, 10, 8, 0),
(10, 'sancy', 'lieux/volcan.png', 45.5284, 2.81393, 10, 10, 0);

-- --------------------------------------------------------

--
-- Table structure for table `objets`
--

CREATE TABLE `objets` (
  `id` int(3) NOT NULL,
  `nom` text NOT NULL,
  `lat` float NOT NULL,
  `longi` float NOT NULL,
  `zoom` float NOT NULL,
  `bloquepar` int(3) NOT NULL,
  `debloque` int(3) NOT NULL,
  `texte` tinyint(1) NOT NULL,
  `cheminacces` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `objets`
--

INSERT INTO `objets` (`id`, `nom`, `lat`, `longi`, `zoom`, `bloquepar`, `debloque`, `texte`, `cheminacces`) VALUES
(1, 'gentiane', 44.1231, 5.8026, 10, 2, 2, 0, 'objets/gentiane.png'),
(2, 'parchemin1', 44.1231, 5.8026, 10, 1, 3, 1, ''),
(3, 'coffre1', 45.8326, 6.86517, 10, 0, 4, 1, 'objets/coffre.png'),
(4, 'mont blanc gateau', 45.8326, 6.86517, 10, 3, 13, 0, 'objets/mont_blanc.png'),
(5, 'bretzel', 48.5819, 7.75104, 10, 4, 14, 0, 'objets/bretzel.png'),
(6, 'charbon', 50.4608, 2.98698, 10, 16, 17, 0, 'objets/charbon.png'),
(7, 'gateau breton', 48.6095, -2.49474, 10, 6, 7, 0, 'objets/gateau_breton.jpg'),
(8, 'canele', 44.8401, -0.598408, 10, 20, 9, 0, 'objets/canele.png'),
(9, 'opera', 48.8584, 2.29448, 10, 18, 8, 0, 'objets/opera.jpg'),
(10, 'escargot', 46.482, 3.98484, 10, 0, 10, 0, 'objets/escargot.jpg'),
(11, 'mont dore', 45.5284, 2.81393, 10, 21, 22, 0, 'objets/mont_dor.jpg'),
(12, 'vin', 46.482, 3.98484, 10, 0, 0, 0, 'objets/vin.png'),
(13, 'parchemin2', 45.8326, 6.86517, 10, 13, 4, 1, ''),
(14, 'insecte', 48.5819, 7.75104, 10, 5, 5, 1, ''),
(15, 'tire-bouchon', 44.8401, -0.598408, 10, 20, 0, 0, 'objets/tire_bouchon.png'),
(16, 'coffre2', 50.4608, 2.98698, 10, 0, 6, 1, 'objets/coffre.png'),
(17, 'parchemin3', 48.5819, 7.75104, 10, 6, 6, 1, ''),
(18, 'femme', 48.8584, 2.29448, 10, 0, 9, 1, ''),
(19, 'homme', 48.8584, 2.29448, 10, 0, 9, 1, ''),
(20, 'prospectus', 44.8401, -0.598408, 10, 0, 8, 1, ''),
(21, 'coffre3', 44.8401, -0.598408, 10, 0, 11, 1, 'objets/coffre.png'),
(22, 'parchemin4', 50.4608, 2.98698, 10, 11, 0, 1, ''),
(23, 'coffre4', 45.5284, 2.81393, 10, 0, 0, 0, 'objets/coffre.png'),
(24, 'julie', 43.9599, 5.78071, 10, 10, 0, 0, 'objets/julie_mure.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `classement`
--
ALTER TABLE `classement`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `classement`
--
ALTER TABLE `classement`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
