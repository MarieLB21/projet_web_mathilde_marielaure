<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Escape Game : inscription</title>
    <link rel="stylesheet" href="css/style_EG.css">
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Courgette|Satisfy&display=swap" rel="stylesheet">
  </head>
  <body>
    <h1>Bonjour !   Bienvenue  dans  l'Escape  Game : Julie  la  petite  Olive </h1>
    <div id="div">
      <form action="Histoire.php" method="post" id="inscription">
      <fieldset id="field">
        <legend id="legend">Inscription</legend>
        <div>
          <p class="info"><label>Pseudo  <input type="text" name="pseudo" id="pseudo"></label></p>
          <p class="info"><label>Email  <input type="text" name="mail" id="mail"></label></p>
          <p><input class="valid" type="submit" name="valider" value="Valider" id="vali"></p>
        </div>
        </fieldset>
      </form>
    </div>
    </body>
</html>
