<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/style_hist.css">
    <link href="https://fonts.googleapis.com/css?family=Courgette|Satisfy&display=swap" rel="stylesheet">
    <title>Escape Game : petite histoire</title>
  </head>
  <body>
    <embed id="music" src="musiques/julie_petite_olive_intro_correct.mp3" autostart="true" loop="false" hidden="true"></embed>
    <form id="hist" action="IntroVersMap.php" method="post">
      <div id="histoire">
        <p id="titre">Histoire :</p>
        <p id="deb">Avant de débuter votre lecture, veuillez cliquez sur play</p>
        <p class="narration">Quelque part dans le Sud-est de la France se trouvait Julie. Son rêve ? Voyager ! Parcourir le monde ! Découvrir les merveilles dont il regorge ! Mais il y avait là un problème, Julie était une olive ! Elle était donc enchaîner à son olivier et incapable de réaliser son rêve…  Elle devait se contenter d’observer le soleil passer tous les jours devant ses yeux, lui qui était libre de découvrir tous les mystères de cette Terre ... Mais au fait, il était là lorsque le monde fut créé, il connait donc toute l’histoire de celui-ci, il pourrait alors en faire le récit à Julie ! Notre petite olive prit son courage à deux feuilles et demanda au soleil de lui décrire cet univers qui l’entoure.</p>
        <p class="julie">Julie : Monsieur le soleil, vous qui avez tout vu et moi qui aimerais tellement voyager, pouvez-vous me décrire ce fabuleux monde qui m’entoure ?</p>
        <p class="soleil">Soleil : Oh mais bien sûr ma chère petite olive ! Mais le monde est vaste et bien vieux cela va prendre beaucoup de temps. Je vais commencer par t’expliquer sa création.</p>
        <p class="narration">C’est à cet instant qu’un majestueux papillon nommé Hatlas, voletant à proximité, entendit cette intéressante conversation. Il se dirigea vers Julie pour lui faire une proposition intéressante.</p>
        <p class="hatlas">Hatlas : Ma belle Julie, j’ai entendu ta conversation avec le Soleil. Si tu le souhaites, je peux parcourir la France à ta place afin de te rapporter différents souvenirs ainsi tu connaîtras certaines choses qui composent cette Terre. De cette manière je pourrais aussi découvrir les belles possessions de ce territoire qui nous accueille le temps d’une vie !</p>
        <p class="julie">Julie : Oh mais en voilà une bonne idée mon très cher Hatlas ! Si tu veux bien parcourir ce monde et me décrire ce que tu y auras vu en ponctuant ton récit de souvenirs des lieux les plus marquants, tu ferais de moi l’olive la plus heureuse de ce monde !</p>
        <p class="hatlas">Hatlas : A votre service, ma chère ! Je pars de ce coup d’aile pour ce fabuleux voyage !</p>
        <p class="julie">Julie : Super ! Mais sois de retour avant l’olivaison !</p>
        <p class="hatlas">Hatlas : Bien évidemment ! Je te laisse au récit du Soleil !</p>
        <p id="suite"><input class="suite" type="submit" name="suite" value="Suite"></p>
      </div>
    </form>
    <?php
    // Ouverture d'une session
    session_start();
    // Stockage du pseudo
    $_SESSION['pseudo']=$_POST['pseudo'];
    // Connexion à la BDD
    include('connect.php');
    // Stockage du pseudo et de l'email du joueur dans la BDD
    if (isset($_POST['pseudo'])){
        $pseudo = $_POST['pseudo'];
        $mail = $_POST['mail'];
        $req ="INSERT INTO classement(pseudo, mail) VALUES ('$pseudo','$mail')";
        mysqli_query($link, $req);
    }
    ?>
  </body>
</html>
