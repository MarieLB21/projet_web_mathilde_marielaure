<?php

// Connexion à la BDD
include('connect.php');



// obtention du type de demande et du demandeur
$json = json_decode(file_get_contents('php://input'), true);
$type = $json["type"];
$demandeur = $json["demandeur"];


if ($type =="importNomCheminL") {

  $sql = "SELECT nom,cheminacces,lat,longi,debloque,bloquepar FROM lieux";
  $nom_chemin = [];
  if ($result = mysqli_query($link, $sql)) {
    while ($ligne = mysqli_fetch_assoc($result)) {
      $nom_chemin[] = $ligne;
    }
  }
  echo json_encode($nom_chemin);
}

if ($type =="importNomCheminO") {

  $sql = "SELECT nom,cheminacces,lat,longi,debloque,bloquepar FROM objets";
  $nom_chemin = [];
  if ($result = mysqli_query($link, $sql)) {
    while ($ligne = mysqli_fetch_assoc($result)) {
      $nom_chemin[] = $ligne;
    }
  }
  echo json_encode($nom_chemin);
}

?>
