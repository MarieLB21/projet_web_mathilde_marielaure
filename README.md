# Escape Game : Julie la petite Olive !



## Les logiciels

* Les logiciels à installer :
    * MAMP[](https://www.mamp.info/en/downloads/)
    * Atom (pour regarder le code)[](https://atom.io/)




* Les logiciels utilisés :
    * phpMyAdmin
    * MAMP
    
* Les navigateurs conseillés : 
    * Firefox
    * Chrome (il peut apparaitre une erreur de $_SESSION, le pseudo ne se stocke pas)


NB : sur deux pages du jeu nommées Histoire.php et fin.php le texte en italique est à prendre en compte pour le navigateur Firefox uniquement

## Avant de tester le site

### Recherche du dossier où déposer le projet
* Lancer MAMP
    * Cliquer sur l'onglet MAMP
    * Préférences
    * Onglet Web Server
    * Regarder Document Root qui contient le nom du dossier avec son chemin où déposer le projet (htdocs par défaut)

### Importer le projet
* Créer un répertoire git dans votre dossier htdocs
    * git clone https://gitlab.com/MarieLB21/projet_web_mathilde_marielaure.git
    * cd projet_web_mathilde_marielaure
    * touch README.md
    * git add README.md
    * git commit -m "add README"
    * git push -u origin master

### Charger la base de données
* Taper localhost/MAMP dans la barre de recherche Firefox
* Cliquer sur le lien PhpMyAdmin
* Créer une nouvelle base de données que vous nommerez "bdd_projet_web"
* Cliquer sur l'onglet importer 
    * aller dans le dossier où se situe le projet
    * ajouter le fichier nommé bdd_projet_web dans le dossier bdd
    * cliquer sur go
Votre base de données est installée

## Lancement du jeu

* Dans la barre de chargement d'url, veuillez rentrer "localhost/Inscription.php".
* S'inscrire en rentrant votre pseudo et email
* Cliquer sur "valider"
* Vous vous trouvez sur la page où l'on met en place le contexte du jeu (la musique ne se met pas en route 
automatiquement sur Firefox mais c'est le cas sur Chrome)
* Après avoir lu le contexte, cliquez sur "suite" 
* Vous vous trouvez sur la page qui vous donnera votre premier indice 
* Cliquez sur "C'est parti"
* Le jeu commence et votre temps est compté


## Déroulement du jeu et indices supplémentaires
 (Si vous ne l'avez pas encore fait, essayez le jeu avant de regarder la partie qui suit car elle
contient toutes les réponses)



* L'olivier est le premier icone du jeu qui apparaît automatique et qui symbolise Forcalquier. Il faut cliquer sur cet icone pour que le suivant puisse 
être créé et apparaître suivant le niveau de zoom (supérieur à 10).

    * indice supplémentaire : Le lieux suivant est située à Saint-Étienne-les-Orgues, son sommet culmine à 1826m.

* C'est donc la montagne de Lure qui apparait par la suite. Lorsque l'on clique sur son icone, une gentiane apparaît, en cliquant dessus l'indice suivant
est dévoilé et la gentiane est stockée dans l'inventaire.

    * indice supplémentaire : Le sommet des Alpes

* Le Mont-Blanc apparaît, cliquez dessus et un coffre apparaît avec une énigme. La hauteur à laquelle se situe le coffre est celle du sommet du Mont-Blanc 
qui est donc 4810m. Lorsque l'on a bien répondu, un gateau apparaît et en cliquant dessus, on obtient l'énigme suivante.

    * indice supplémentaire : Le plus haut monument religieux de la plus grande ville faisant frontière avec l’Allemagne.

* Il s'agit de Strasbourg, précisément la cathédrale de Strasbourg, cliquez et obtenez un bretzel et l'énigme suivante.

    * indice supplémentaire : La dernière mine du Nord-Pas-de-Calais à être fermée.

* Rendez-vous à Oignies, dernière mine à avoir été fermée. en cliquant dessus un coffre apparaît avec son énigme. Emile Zola s'est inspiré du domaine minier 
pour écrire Germinal, 13ème roman de la série des Rougon-Macquart. Du charbon apparaît avec l'énigme.

    * indice supplémentaire : Situé sur les côtes nord de Bretagne

* Rendez-vous à Erquy, le long des côtes. Cliquez un gâteau breton apparaît avec l'énigme.

    * indice supplémntaire : grande dame de fer

* Bienvenue à la Tour Eiffel, cliquez dessus, l'énigme suivante apparaît, cliquer sur l'opéra pour le stocker dans votre inventaire.

    * indice supplémentaire : La cadillac du Corniaud de Gérard Oury a fini son périple dans cette ville.

* Voici Bordeaux et une épicerie, cliquez dessus un coffre apparaît et sa solution est le code INSEE de Bordeaux qui est 33063. Un canelé apparaît, cliquez un
tire-bouchon apparaît avec l'énigme.

    * indice supplémentaire : ville de Saône et Loire.

* Rendez-vous à Digoin, où en cliquant sur le chapiteau vous obtenez des escargots et du vin ainsi que l'énigme suivante.

    * indice supplémentaire : Je suis le plus haut sommet de la chaîne des Puy

* Montez jusqu'au sommet du Puy de Sancy pour trouver le dernier coffre où la réponse à son énigme est "Puy de Dôme" (sans les guillemets). Vous obtenez un Mont-Dore,
cliquez dessus et vous aurez la dernière énigme.

    * indice supplémentaire : Vous en avez vraiment besoin ? Bon aller,  c'est une ville des Alpes-de-Haute-Provence commençant par un F.

* Vous voici donc de retour sur Forcalquier où vous finissez votre périple en cliquant sur Julie notre petite olive qui est devenue noire car elle a mûrie.
    * Cliquez dessus et vous serez envoyez vers la page où il y a la dernière question pour que Julie puisse obtenir ses présents. La réponse est Novembre.

    * Cliquez sur valider et vous connaitrez alors la fin de l'histoire de notre chère Julie.

* Et ainsi vous aurez aussi accès à l'ultime page où vous trouverez votre score (si vous avez utilisé Firefox)

